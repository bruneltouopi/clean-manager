import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Building } from '../../domains/Building';

@Injectable()
export class AssetCheckListService {
    constructor(private http: Http) {}

    getBuildingList() {
        debugger;
        return this.http.get('../../../assets/data/building.json')
        .toPromise()
        .then(res => <Building[]> res.json().data)
        .then(data => data);
    }
}
