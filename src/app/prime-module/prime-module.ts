import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from '@angular/forms';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import {CardModule} from 'primeng/card';
import {TableModule} from 'primeng/table';
import {PaginatorModule} from 'primeng/paginator';

@NgModule({
  imports: [
    CommonModule, FormsModule
    , InputTextModule, ButtonModule, CardModule,
     TableModule, PaginatorModule
  ],
  exports: [FormsModule
    , InputTextModule, ButtonModule, CardModule, TableModule],
  declarations: []
})
export class PrimeModule {}
