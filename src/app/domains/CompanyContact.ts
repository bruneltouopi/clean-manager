import { BaseEntity } from './BaseEntity';

export const enum ContactType {
    'BILLING',
    'MAIN_CONTACT',
    'LEGAL',
    'SECONDARY',
    'CLERK',
    'ACCOUNT',
    'HOUSE_KEEPING',
    'WORKPLACE',
    'HR',
    'TRANSPORT'
}

export class CompanyContact implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public surname?: string,
        public phone?: string,
        public mobile?: string,
        public faxNumber?: string,
        public email?: string,
        public roleDescription?: string,
        public comments?: string,
        public contactType?: ContactType,
        public createDate?: any,
        public modifiedDate?: any,
        public userId?: number,
        public companyId?: number,
    ) {
    }
}
