import { BaseEntity } from './BaseEntity';

export const enum DepartementType {
    'CLEANER',
    'PAINT',
    'GARDENING',
    'SECURITY',
    'MAINTENANCE',
    'ACCOUNTING',
    'SUPEVISION',
    'MANAGEMENT',
    'LEGAL'
}

export class Employee implements BaseEntity {
    constructor(
        public id?: number,
        public enterpriseId?: string,
        public firstName?: string,
        public lastName?: string,
        public email?: string,
        public phoneNumber?: string,
        public hireDate?: any,
        public salary?: number,
        public commissionPct?: number,
        public departement?: DepartementType,
        public userId?: number,
    ) {
    }
}
