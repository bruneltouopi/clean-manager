import { BaseEntity } from './BaseEntity';

export const enum ChecklistStatusType {
    'OK',
    'KO_REPAIRS_REQUIRED',
    'KO_UNKNOWN',
    'NEW',
    'KO_REPAIRS_INPROGRESS'
}

export class AssetCheckList implements BaseEntity {
    constructor(
        public id?: number,
        public generalComment?: string,
        public checklistStatusType?: ChecklistStatusType,
        public assetId?: number,
        public floorSpaceChecklistId?: number,
    ) {
    }
}
