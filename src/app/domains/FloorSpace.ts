import { BaseEntity } from './BaseEntity';

export const enum FloorSpaceType {
    'MESSROOM',
    'WASHROOM',
    'SHOWER',
    'MEETING_ROOM',
    'STORAGE',
    'SERVER_ROOM',
    'FLOOR_AREA'
}

export class FloorSpace implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public seating?: number,
        public surfaceArea?: number,
        public floorSpaceType?: FloorSpaceType,
        public assets?: BaseEntity[],
        public workschedules?: BaseEntity[],
        public floorId?: number,
    ) {
    }
}
