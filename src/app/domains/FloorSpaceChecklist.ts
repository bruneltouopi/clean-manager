import { BaseEntity } from './BaseEntity';

export const enum ChecklistStatusType {
    'OK',
    'KO_REPAIRS_REQUIRED',
    'KO_UNKNOWN',
    'NEW',
    'KO_REPAIRS_INPROGRESS'
}

export class FloorSpaceChecklist implements BaseEntity {
    constructor(
        public id?: number,
        public checklistDate?: any,
        public clean?: boolean,
        public cleanlinessDescription?: string,
        public generalComments?: string,
        public checklistStatusType?: ChecklistStatusType,
        public createDate?: any,
        public modifiedDate?: any,
        public assetCheckLists?: BaseEntity[],
        public assignedToId?: number,
        public workscheduleId?: number,
    ) {
        this.clean = false;
    }
}
