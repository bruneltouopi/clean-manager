import { BaseEntity } from './BaseEntity';

export class Building implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public contactName?: string,
        public contactSurname?: string,
        public address?: string,
        public primaryPhoneNumber?: string,
        public secondaryPhoneNumber?: string,
        public faxNumber?: string,
        public floors?: BaseEntity[],
        public companyId?: number
    ) {
    }
}
