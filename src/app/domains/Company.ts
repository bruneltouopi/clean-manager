import { BaseEntity } from './BaseEntity';

export class Company implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public address?: string,
        public primaryPhoneNumber?: string,
        public secondaryPhoneNumber?: string,
        public faxNumber?: string,
        public buildings?: BaseEntity[],
        public contacts?: BaseEntity[],
    ) {
    }
}
