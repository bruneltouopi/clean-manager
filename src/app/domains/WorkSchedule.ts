import { BaseEntity } from './BaseEntity';

export class WorkSchedule implements BaseEntity {
    constructor(
        public id?: number,
        public createDate?: any,
        public modifiedDate?: any,
        public title?: string,
        public description?: string,
        public effectiveFrom?: any,
        public disactivateOn?: any,
        public checklists?: BaseEntity[],
        public employeeId?: number,
        public floorSpaceId?: number,
    ) {
    }
}
