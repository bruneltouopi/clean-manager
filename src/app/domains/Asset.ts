import { BaseEntity } from './BaseEntity';

export const enum AssetType {
    'WORKSTATION',
    'PRINTER',
    'FAX',
    'AC',
    'TUBELIGHT',
    'SPOTLIGHT',
    'SOCKET',
    'PLANT',
    'STATIONARY',
    'SINK',
    'TV',
    'VIDEO_CONF',
    'DISPENSER',
    'FITTINGS',
    'TABLE',
    'CHAIR',
    'COFFEE',
    'MILK',
    'WATER_DISPENSOR',
    'CEILING_FAN',
    'FAN'
}

export class Asset implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public barcode?: string,
        public description?: string,
        public asset?: AssetType,
        public assetCheckLists?: BaseEntity[],
        public floorSpaceId?: number,
    ) {
    }
}
