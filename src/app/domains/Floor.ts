import { BaseEntity } from './BaseEntity';

export class Floor implements BaseEntity {
    constructor(
        public id?: number,
        public number?: number,
        public name?: string,
        public floorSpaces?: BaseEntity[],
        public buildingId?: number,
    ) {
    }
}
