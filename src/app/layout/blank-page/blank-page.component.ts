import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import {Building} from '../../domains/Building';
import { AssetCheckListService } from '../../shared/services/asset-check-list.services';

@Component({
    selector: 'app-blank-page',
    templateUrl: './blank-page.component.html',
    styleUrls: ['./blank-page.component.scss'],
    animations: [routerTransition()]
})
export class BlankPageComponent implements OnInit {

    selectedBuilding: Building;
    buildings: Building[];
    cols: any[];
    constructor(private assetCheckListService: AssetCheckListService) {}

    ngOnInit() {
        this.assetCheckListService.getBuildingList().then(building => this.buildings = building);
        this.cols = [
            { field: 'name', header: 'name' },
            { field: 'contactName', header: 'Contact Names' },
            { field: 'primaryPhoneNumber', header: 'PhoneNumber' },
        ];
    }
}
