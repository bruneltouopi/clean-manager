import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import {HttpModule} from '@angular/http';

import { BlankPageRoutingModule } from './blank-page-routing.module';
import { BlankPageComponent } from './blank-page.component';
import { PrimeModule } from '../../prime-module/prime-module';
import { CardModule } from 'primeng/card';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { FormsModule } from '@angular/forms';
import { AssetCheckListService } from '../../shared/services/asset-check-list.services';


@NgModule({
    imports: [CommonModule, BlankPageRoutingModule, PrimeModule, HttpClientModule, HttpModule],
    declarations: [BlankPageComponent],
    providers: [AssetCheckListService]
})
export class BlankPageModule {}
