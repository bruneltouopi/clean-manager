### Introduction
This project provides the following features:

- Developed using boostrap-v4.0.0
- angular-v5.2.4
- angular/cli-v1.6.8
- [ng-bootstrap-v1.0.0](https://github.com/ng-bootstrap/)
- [ngx-translate-v9.1.1](https://github.com/ngx-translate)
- [prime-ng](https://www.primefaces.org/primeng/#/)
- Following the best practices.
- Ahead-of-Time compilation support.
- Official Angular i18n support.
- Production and development builds.
- Tree-Shaking production builds.

### How to start
**Note** that this seed project requires  **node >=v6.9.0 and npm >=3**.

In order to start the project use:

- `bash`
- $ `git clone //http remote repository git`
- $ `cd clean-manager`
install the project's dependencies
- $ `npm install`
 watches your files and uses livereload by default run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
- $ npm start
prod build, will output the production application in `dist`
 the produced code can be deployed (rsynced) to a remote server
- $ `npm run build`

### Running unit tests
- Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Running end-to-end tests

- Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.


### Running using Docker
Create the docker image
- Run `docker build . --tag clean-manager:v0.1`

### RUN the app
- Run `docker run --name angular4-docker-example -d -p 80:80 clean-manager:v0.1`

- Open the [browser](http://localhost:80)